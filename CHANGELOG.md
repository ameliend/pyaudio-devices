## [1.0.0](https://gitlab.com/ameliend/pyaudio-devices/compare/...v1.0.0) (2024-11-01)

### 📔 Docs

* **README:** update README ([4f10f06](https://gitlab.com/ameliend/pyaudio-devices/commit/4f10f065b56bfc2336baa1ee879452bba6591be0))
* **sphinx:** removed sphinx docs ([569f733](https://gitlab.com/ameliend/pyaudio-devices/commit/569f733658782753e463a6e6dfb929125a4b88a8))

### 🦊 CI/CD

* **setup:** added dependencies, update classifiers, removed docs ([2181211](https://gitlab.com/ameliend/pyaudio-devices/commit/2181211a7b17bba63dbcd86cf7ada911858555f2))

### 🧪 Tests

* removed tests ([0ec7328](https://gitlab.com/ameliend/pyaudio-devices/commit/0ec73280ded9e873e8c93250455a558e7b0eca57))

### 🚀 Features

* initial commit ([6ec9573](https://gitlab.com/ameliend/pyaudio-devices/commit/6ec957373f787a3072ec16cbad3fb938379faecb))
* initial commit ([f3ce909](https://gitlab.com/ameliend/pyaudio-devices/commit/f3ce909342750bfab17729cde35b26a5ea27a324))
