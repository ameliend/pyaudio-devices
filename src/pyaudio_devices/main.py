"""Main module."""

from __future__ import annotations

from typing import Literal

import pyaudio

p = pyaudio.PyAudio()


def get_devices(device_type: Literal["input", "output"]) -> list:
    """Return a list of compatible devices based on the given device type.

    Parameters
    ----------
    device_type : Literal
        The type of device (input or output).

    Returns
    -------
    list
        A list of pyaudio devices dict.
    """
    api_info, api_index = _get_preferred_api_info(device_type)
    devices = []
    for index in range(api_info.get("deviceCount")):
        device = p.get_device_info_by_host_api_device_index(api_index, index)
        if device_type == "input":
            max_channels = device.get("maxInputChannels")
        elif device_type == "output":
            max_channels = device.get("maxOutputChannels")
        else:
            msg = "device_type must be 'input' or 'output'"
            raise AttributeError(msg)
        if max_channels > 0 and "Microsoft Sound Mapper" not in device.get("name"):
            devices.append(device)
    return sorted(devices, key=lambda d: d["name"])


def _get_preferred_api_info(
    device_type: Literal["input", "output"],
) -> tuple[pyaudio._PaHostApiInfo | None, int]:
    host_api_name = "MME" if device_type == "input" else "Windows WASAPI"
    for index in range(p.get_host_api_count()):
        current_api_info = p.get_host_api_info_by_index(index)
        if current_api_info["name"] == host_api_name:
            return current_api_info, index
    return None, 0
