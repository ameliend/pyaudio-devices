"""Top-level package for PyAudio Devices."""

from pyaudio_devices._version import __version__
from pyaudio_devices.main import get_devices

__author__ = """Amelien Deshams"""
__email__ = "a.deshams+git@slmail.me"
__all__ = [
    "__version__",
    "get_devices",
]
