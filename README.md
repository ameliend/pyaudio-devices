[![coverage](https://gitlab.com/ameliend/pyaudio-devices/badges/main/coverage.svg)](https://gitlab.com/ameliend/pyaudio-devices/-/commits/main)
[![vscode-editor](https://badgen.net/badge/icon/visualstudio?icon=visualstudio&label)](https://code.visualstudio.com/)
[![uv](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/uv/main/assets/badge/v0.json)](https://github.com/astral-sh/uv)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)
[![Copier](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/copier-org/copier/master/img/badge/badge-grayscale-inverted-border-orange.json)](https://github.com/copier-org/copier)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

# PyAudio Devices

<p align="center">
  <img src="./resources/logo.png">
</p>

Provides functions for interacting with audio devices using Pyaudio.

Used with:

  - [VRChat Speech Assistant](https://gitlab.com/ameliend/vrchat-speech-assistant)
  - [LolaBeta](https://gitlab.com/ameliend/LolaBeta)
